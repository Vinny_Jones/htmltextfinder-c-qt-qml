#ifndef MAINLOGIC_H
# define MAINLOGIC_H

# include <QObject>
# include <QThreadPool>
# include <QDesktopServices>
# include <memory>
# include "webPageModel.h"
# include "currentState.h"
# include "domainmode.h"
# include "searchMode.h"

/* Qml singleton, which sotres/operates data model, global options */
class MainLogic : public QObject
{
	Q_OBJECT
    Q_PROPERTY(int maxThreadCount READ maxThreadCount WRITE setMaxThreadCount NOTIFY maxThreadCountChanged)
    Q_PROPERTY(QAbstractListModel * webPageModel READ webPageModel NOTIFY webPageModelChanged)
    Q_PROPERTY(CurrentState::Type currentState READ currentState WRITE setCurrentState NOTIFY currentStateChanged)
	Q_PROPERTY(SearchMode::Type searchMode MEMBER m_searchMode)
	Q_PROPERTY(DomainMode::Type domainMode MEMBER m_domainMode)
    Q_PROPERTY(bool caseInsensitive MEMBER m_caseInsensitive)
    Q_PROPERTY(int maxPagesAmount MEMBER m_maxPagesAmount)
    Q_PROPERTY(int totalPages MEMBER m_totalPages NOTIFY totalPagesChanged)
    Q_PROPERTY(int finishedPages MEMBER m_finishedPages NOTIFY finishedPagesChanged)
    Q_PROPERTY(int finishedPagesWithResults MEMBER m_finishedPagesWithResults NOTIFY finishedPagesWithResultsChanged)

public:

    static void registerQmlType(const char * uri, int major = 1, int minor = 0);

    explicit MainLogic(QObject *parent = Q_NULLPTR);
    virtual ~MainLogic();

    Q_INVOKABLE void        openLinkInBrowser(const QString & url) const;
    Q_INVOKABLE void        startSearch(const QString & text, const QString & initialUrl);
    Q_INVOKABLE void        toStartState(QString message, bool clearModel = true);

    int                     maxThreadCount() const;
    void                    setMaxThreadCount(int value);
    QAbstractListModel *    webPageModel(void);
    CurrentState::Type      currentState() const;
    void                    setCurrentState(CurrentState::Type);

signals:
    void                    maxThreadCountChanged(int maxThreadCount);
    /* Here unused, because pointer to model never changes */
	void                    webPageModelChanged();
    void                    currentStateChanged();
    void                    statusBarMessageChanged(QString message);
    void                    totalPagesChanged(int amount);
    void                    finishedPagesChanged(int amount);
    void                    finishedPagesWithResultsChanged(int amount);

public slots:
    void                    proceedBFSSearch();
    void                    proceedNewChild(QString url);

private:
	WebPage::Pointer                addElementToModel(const QString & url);

    CurrentState::Type              m_currentState;
    int                             m_maxPagesAmount;
    int                             m_totalPages;
    int                             m_finishedPages;
    int                             m_finishedPagesWithResults;
	QString							m_targetText;
	SearchMode::Type				m_searchMode;
	DomainMode::Type                m_domainMode;
    bool                            m_caseInsensitive;
	QString							m_siteDomain;
    /* Needed for BFS search. Stores current level pages and their children (next level) */
    WebPage::List                   m_activePages;
    std::unique_ptr<WebPageModel>   m_dataModel;
};

#endif
