#ifndef WEBPAGE_H
# define WEBPAGE_H

# include <QObject>
# include <QRegularExpression>
# include <QPointer>
# include <memory>
# include "networkConnection.h"
# include "webPageState.h"
# include "searchMode.h"

/* Defines base model item - web page. Creates connection, downloads HTML code, parses it, sets appropriate state */
class WebPage : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int index MEMBER m_index CONSTANT)
    Q_PROPERTY(QString url READ url CONSTANT)
	Q_PROPERTY(WebPageState::Type pageState READ pageState WRITE setState NOTIFY webPageDataChanged)
    Q_PROPERTY(int resultsFound READ resultsFound NOTIFY resultsFoundChanged)
    Q_PROPERTY(QString errorString READ errorString)

public:
    using Pointer = std::shared_ptr<WebPage>;
    using List = QList<Pointer>;

    WebPage(int index, const QString & text, SearchMode::Type searchMode, bool caseInsensitive, const QString & url, QObject *parent = Q_NULLPTR);
    virtual ~WebPage() = default;

    static void         trimUrlString(QString & url);
	static void			clearUrlString(QString & url);
	static QString		getSiteDomain(const QString & url);
    QString             url() const;
    WebPageState::Type  pageState() const;
    int                 resultsFound() const;
    QString             errorString() const;
    void                setState(WebPageState::Type newValue);
    bool                isFinished() const;
	void                parseWebPage();
    void                setPaused(bool paused);

signals:
	void                webPageDataChanged(int index);
    void                childFound(QString url);
    void                finished();
    void                resultsFoundChanged();

public slots:
    void                setStarted();
    void                connectionFinished(NetworkConnection::Result connectionResult, QString data);

private:
	void				parseForText();
	QString				resolveTargetTextPattern();
	void				parseForChildLinks();
	QString				extractBaseUrl(QRegularExpression & extractLinkFromTag);
    static bool         urlExceptionCases(QString & url);

    const int                   m_index;
    const QString               m_requestedText;
    const SearchMode::Type		m_searchMode;
    const bool                  m_caseInsensitive;
    const QString               m_url;
    QString                     m_errorString;
    int                         m_resultsFound;
    WebPageState::Type          m_state;
    QString                     m_connectionReplyText;
    QPointer<NetworkConnection> m_connection;
};

#endif
