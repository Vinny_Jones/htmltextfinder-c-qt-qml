#ifndef WEBPAGEMODEL_H
# define WEBPAGEMODEL_H

# include <QAbstractListModel>
# include "webPage.h"
# include <deque>

/* Stores model to display web pages in qml */
class WebPageModel : public QAbstractListModel
{
    Q_OBJECT
public:
    static constexpr int FirstPropertyRole = Qt::UserRole + 1;

    WebPageModel(QObject *parent = Q_NULLPTR);
    virtual ~WebPageModel() = default;

    QVariant                data(const QModelIndex & index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray>  roleNames() const override;
    int                     rowCount(const QModelIndex & parent) const override;

    void                    clear();
    WebPage::Pointer &      addElement(const QString & text, SearchMode::Type searchMode, bool caseInsensitive, const QString & url);
    bool                    contains(QString url) const;

private slots:
    void                    dataElementChanged(int index);

private:
    WebPage::List           m_data;
    QHash<int, QByteArray>  m_roles;
};

#endif
