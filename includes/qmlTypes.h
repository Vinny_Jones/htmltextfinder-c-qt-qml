#ifndef QMLTYPES_H
# define QMLTYPES_H

# include <QObject>
# include "enums/currentState.h"
# include "enums/webPageState.h"
# include "enums/searchMode.h"
# include "enums/domainmode.h"
# include "mainLogic.h"
# include "networkConnection.h"


class QmlTypes : public QObject
{
    Q_OBJECT
public:
    static constexpr const char * EnumUri = "Enums";
    static constexpr const char * MainLogicUri = "Logic";

    static void registerTypes()
    {
        CurrentState::registerQmlType(EnumUri);
        SearchMode::registerQmlType(EnumUri);
        WebPageState::registerQmlType(EnumUri);
        DomainMode::registerQmlType(EnumUri);
        MainLogic::registerQmlType(MainLogicUri);
        qRegisterMetaType<NetworkConnection::Result>("Result");
    }
};

#endif
