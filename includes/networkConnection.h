#ifndef NETWORKCONNECTION_H
# define NETWORKCONNECTION_H

# include <QObject>
# include <QRunnable>
# include <QThreadPool>
# include <QEventLoop>
# include <QNetworkAccessManager>
# include <QNetworkReply>
# include <QTimer>
# include <QMutex>

/* Creates network connection for WebPage object, returns HTML code. Used in separate thread. */
class NetworkConnection : public QObject, public QRunnable
{
    Q_OBJECT
public:
    Q_DISABLE_COPY(NetworkConnection)

    static constexpr int    connectionTimeoutMS = 2000;

    enum class Result
    {
        SUCCESS = 0,
        TIMEOUT,
        ERROR
    };
    Q_ENUM(Result)

    NetworkConnection(const QUrl & url, QObject *parent = Q_NULLPTR);
    virtual ~NetworkConnection() = default;

    virtual void    run();
    void            setPaused(bool paused);

signals:
    void            started();
    void            finished(NetworkConnection::Result connectionResult, QString data);

private:
    QMutex  m_mutex;
    QUrl    m_url;
    bool    m_paused;
};

#endif
