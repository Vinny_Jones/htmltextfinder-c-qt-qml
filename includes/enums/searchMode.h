#ifndef SEARCHMODE_H
# define SEARCHMODE_H

#include <QObject>
#include <QQmlEngine>

class SearchMode : public QObject {
    Q_OBJECT
public:
    enum class Type {
        WHOLE = 0,
        SEPARATE = 1
    };
    Q_ENUM(Type)

    static void registerQmlType(const char *uri, int major = 1, int minor = 0, const char *message = "Can't create instance of enum")
    {
        qmlRegisterUncreatableType<SearchMode>(uri, major, minor, "SearchMode", message);
    }
};

#endif
