#ifndef CURRENTSTATE_H
# define CURRENTSTATE_H

#include <QObject>
#include <QQmlEngine>

/* Enum for current state of program */
class CurrentState : public QObject {
    Q_OBJECT
public:
    enum class Type {
        RUNNING = 0,
        PAUSE = 1,
        IDLE = 2
    };
    Q_ENUM(Type)

    static void registerQmlType(const char *uri, int major = 1, int minor = 0, const char *message = "Can't create instance of enum")
    {
        qmlRegisterUncreatableType<CurrentState>(uri, major, minor, "CurrentState", message);
    }
};

#endif
