#ifndef WEBPAGESTATE_H
# define WEBPAGESTATE_H

# include <QObject>
# include <QQmlEngine>

/* Enum for current state of web page item */
class WebPageState : public QObject {
    Q_OBJECT
public:
    enum class Type
    {
        IDLE = 0,
        STARTED = 1,
        FINISHED_SUCCESSFUL = 2,
        FINISHED_ERROR = 3
    };
    Q_ENUM(Type)

    static void registerQmlType(const char *uri, int major = 1, int minor = 0, const char *message = "Can't create instance of enum")
    {
        qmlRegisterUncreatableType<WebPageState>(uri, major, minor, "WebPageState", message);
    }
};

#endif
