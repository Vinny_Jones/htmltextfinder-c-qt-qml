#ifndef DOMAINMODE_H
# define DOMAINMODE_H

#include <QObject>
#include <QQmlEngine>

class DomainMode : public QObject {
    Q_OBJECT
public:
    enum class Type {
        INTERNAL = 0,
        EXTERNAL = 1
    };
    Q_ENUM(Type)

    static void registerQmlType(const char *uri, int major = 1, int minor = 0, const char *message = "Can't create instance of enum")
    {
        qmlRegisterUncreatableType<DomainMode>(uri, major, minor, "DomainMode", message);
    }
};

#endif
