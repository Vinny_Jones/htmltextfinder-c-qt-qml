import QtQuick 2.6
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.0
import Logic 1.0
import Enums 1.0

Button {
    Layout.fillWidth: true
    Layout.fillHeight: true
    Layout.alignment: Qt.AlignHCenter
    Layout.leftMargin: 10
    Layout.rightMargin: 10

    text: "Button"
}
