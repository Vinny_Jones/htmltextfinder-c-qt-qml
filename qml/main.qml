import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0
import QtQuick.Window 2.0
import Logic 1.0
import Enums 1.0

ApplicationWindow {
    id: root
    title: "Find text on website pages"
	visible: true
	width: 500
    minimumWidth: width
    maximumWidth: width
    height: 800
    minimumHeight: height
    maximumHeight: height

    onClosing: MainLogic.toStartState("")

    Rectangle {
        id: _rootRect
        width: parent.width
        height: parent.height
        color: "#f3f3f4"

        Column {
            id: _headerColumn
            anchors.top: _rootRect.top
            anchors.left: _rootRect.left
            anchors.right: _rootRect.right
            anchors.margins: 10
            spacing: 5

            Grid {
                id: _topGrid
                width: _headerColumn.width
                columns: 2
                spacing: 10

                Text {
                    text: "Search for: "
                }

                TextField {
                    id: _requestedText
                    width: 390
                    text: "About"
                    enabled: MainLogic.currentState == CurrentState.IDLE
                }

                Text {
                    text: "Start url: "
                }

                TextField {
                    id: _targetUrl
                    width: 390
                    text: "https://helpx.adobe.com/support.html"
                    enabled: MainLogic.currentState == CurrentState.IDLE
                }
            }

            RowLayout {
                id: _buttonsRow
                width: _headerColumn.width
                height: 40
                Layout.alignment: Qt.AlignHCenter
                spacing: 5

                Rectangle {
                    Layout.preferredWidth: 310
                    Layout.fillHeight: true
                    color: "transparent"

                    RowLayout {
                        anchors.fill: parent
                        spacing: 5

                        Text {
                            Layout.fillWidth: true
                            text: "Max threads:"
                        }

                        TextField {
                            id: _maxThreadNumber
                            Layout.preferredWidth: 40
                            text: "5"
                            validator: IntValidator { bottom: 1 }
                            enabled: MainLogic.currentState == CurrentState.IDLE
                        }

                        Text {
                            Layout.fillWidth: true
                            text: "Max web pages:"
                        }

                        TextField {
                            id: _maxPageAmount
                            Layout.preferredWidth: 60
                            text: "200"
                            validator: IntValidator { bottom: 1 }
                            enabled: MainLogic.currentState == CurrentState.IDLE
                        }
                    }
                }

                ControlButton {
                    text: {
                        switch (MainLogic.currentState) {
                        case CurrentState.IDLE:
                            return ("Run")
                        case CurrentState.RUNNING:
                            return ("Pause");
                        case CurrentState.PAUSE:
                            return ("Resume");
                        default:
                            return ("");
                        }
                    }
                    onClicked: {
                        if (_requestedText.text.length < 3 || _targetUrl.text.length < 3)
                            return;
                        switch (MainLogic.currentState) {
                        case CurrentState.IDLE:
                            MainLogic.maxThreadCount = Number(_maxThreadNumber.text);
                            MainLogic.maxPagesAmount = Number(_maxPageAmount.text);
                            MainLogic.startSearch(_requestedText.text, _targetUrl.text)
                            return;
                        case CurrentState.PAUSE:
                            MainLogic.currentState = CurrentState.RUNNING;
                            return;
                        case CurrentState.RUNNING:
                            MainLogic.currentState = CurrentState.PAUSE;
                            return;
                        }
                    }
                }

                ControlButton {
                    text: "Stop"
                    visible: MainLogic.currentState != CurrentState.IDLE
                    onClicked: MainLogic.toStartState("Stopped")
                }
            }

            RowLayout {
                id: _searchAndMaxPagesOptionsRow
                width: _headerColumn.width
                Layout.alignment: Qt.AlignHCenter
                spacing: 10

                ColumnLayout {
                    Layout.preferredWidth: (_buttonsRow.width - _buttonsRow.spacing) / 2
                    Layout.fillHeight: true

                    GridLayout {
                        Layout.fillWidth: true
                        columns: 2
                        columnSpacing: 5
                        rowSpacing: 5

                        Text {
                            id: _stausBarText
                            Layout.preferredWidth: 100
                            text: ""
                            font.bold: true
                        }

                        Text {
                            id: _foundAmountText
                            text: "Pages found: <b>" + String(MainLogic.finishedPagesWithResults) + "</b>"
                        }
                    }

                    ProgressBarWithNumbers {
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                    }

                    Connections {
                        target: MainLogic
                        onStatusBarMessageChanged: _stausBarText.text = message;
                    }
                }

                GroupBox {
                    id: _searchOptions
                    title: "Search options"
                    Layout.preferredWidth: (_searchAndMaxPagesOptionsRow.width - _searchAndMaxPagesOptionsRow.spacing) / 2
                    enabled: MainLogic.currentState == CurrentState.IDLE

                    Row {
                        ExclusiveGroup { id: _searchOptionsGroup }
                        spacing: 10

                        RadioButton {
                            id: _wholeSearchModeRadio
                            text: "Whole phrase"
                            exclusiveGroup: _searchOptionsGroup
                            onCheckedChanged: {
                                if (checked)
                                    MainLogic.searchMode = SearchMode.WHOLE
                            }
                        }
                        RadioButton {
                            text: "Any word"
                            exclusiveGroup: _searchOptionsGroup
                            onCheckedChanged: {
                                if (checked)
                                    MainLogic.searchMode = SearchMode.SEPARATE
                            }
                        }
                        Component.onCompleted: _wholeSearchModeRadio.checked = true
                    }
                }
            }

            RowLayout {
                id: _domainOptionsAndCaseSensivityRow
                width: _headerColumn.width
                Layout.alignment: Qt.AlignHCenter
                spacing: 10

                GroupBox {
                    id: _domainOptions
                    title: "Domain options"
                    Layout.preferredWidth: 250
                    enabled: MainLogic.currentState == CurrentState.IDLE

                    Row {
                        ExclusiveGroup { id: _domainOptionsGroup }
                        spacing: 10

                        RadioButton {
                            id:_currentDomainOptionRadio
                            text: "Within domain"
                            exclusiveGroup: _domainOptionsGroup
                            onCheckedChanged: {
                                if (checked)
                                    MainLogic.domainMode = DomainMode.INTERNAL
                            }
                        }
                        RadioButton {
                            text: "Include external"
                            exclusiveGroup: _domainOptionsGroup
                            onCheckedChanged: {
                                if (checked)
                                    MainLogic.domainMode = DomainMode.EXTERNAL
                            }
                        }
                        Component.onCompleted: _currentDomainOptionRadio.checked = true
                    }
                }

                GroupBox {
                    Layout.fillWidth: true
                    anchors.bottom: parent.bottom

                    ExclusiveGroup { id: _caseSensitiveCheckbox }

                    CheckBox {
                        text: "Case insensitive"
                        enabled: MainLogic.currentState == CurrentState.IDLE
                        onCheckedChanged: MainLogic.caseInsensitive = Boolean(checked);
                        Component.onCompleted: checked = true
                    }
                }
            }
        }

        ListView {
            id: _mainListView
            anchors.top: _headerColumn.bottom
            anchors.left: _rootRect.left
            anchors.right: _rootRect.right
            anchors.bottom: _rootRect.bottom
            anchors.margins: 10
            verticalLayoutDirection: ListView.TopToBottom
            spacing: 2
            clip: true

            model: MainLogic.webPageModel

            ScrollBar.vertical: ScrollBar {}

            delegate: WebPageDelegate {
                width: _mainListView.width
                height: 50
                currentPageState: pageState
                pageUrl: url
                totalResultsFound: resultsFound
                errorStringResolved: errorString
            }
        }
    }

    Component.onCompleted: {
        setX(Screen.width / 2 - width / 2);
        setY(Screen.height / 2 - height / 2);
    }
}
