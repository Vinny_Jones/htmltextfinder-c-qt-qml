import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0
import Logic 1.0

Rectangle {
    id: _root
    Layout.fillWidth: true
    color: "transparent"

    ProgressBar {
        id: _progressBar
        anchors.centerIn: _root
        minimumValue: 0

        Connections {
            target: MainLogic
            onFinishedPagesChanged: { _progressBar.value = amount }
            onTotalPagesChanged: { _progressBar.maximumValue = amount }
        }
    }

    Rectangle {
        anchors.fill: _progressBar
        color: "transparent"

        Text {
            id: _foundStatsText
            anchors.centerIn: parent
            text: String(MainLogic.finishedPages) + " / " + String(MainLogic.totalPages)
            font.pixelSize: 10
            font.bold: true
        }
    }
}
