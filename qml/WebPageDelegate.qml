import QtQuick 2.6
import QtQuick.Controls 1.4
import Logic 1.0
import Enums 1.0

Rectangle {
    id: _root
    border.width: 1
    border.color: "grey"
    radius: 10
    color: "transparent"

    property int currentPageState
    property string pageUrl
    property int totalResultsFound
    property string errorStringResolved

    Text {
        id: _pageUrl
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: _stateText.left
        anchors.margins: 5
        maximumLineCount: 1
        elide: Text.ElideMiddle
        text: pageUrl
    }

    Text {
        id: _stateText
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: 5
        font.pixelSize: 13
        font.bold: true
        text: {
            switch (currentPageState) {
            case WebPageState.IDLE:
                return "Pending...";
            case WebPageState.STARTED:
                if (MainLogic.currentState == CurrentState.PAUSE)
                    return "Paused...";
                return "Downloading...";
            case WebPageState.FINISHED_ERROR:
                return "Finished with error";
            case WebPageState.FINISHED_SUCCESSFUL:
                return "Finished";
            }
        }
    }

    Text {
        id: _foundText
        anchors.top: _pageUrl.bottom
        anchors.topMargin: 5
        anchors.left: _root.left
        anchors.leftMargin: 20
        anchors.right: _openLinkInBrowserButton.left
        anchors.rightMargin: 10
        maximumLineCount: 1
        elide: Text.ElideRight
        font.bold: true
        font.pixelSize: 14
        text: ""
    }

    Button {
        id: _openLinkInBrowserButton
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 3
        width: 80
        visible: currentPageState != WebPageState.IDLE

        text: "Open link"

        onClicked: MainLogic.openLinkInBrowser(pageUrl)
    }

    Behavior on height {
        NumberAnimation { duration: 500 }
    }

    states: [
        State {
            when: _root.currentPageState == WebPageState.STARTED
            PropertyChanges {
                target: _root
                color: "lightblue"
            }
        },
        State {
            when: _root.currentPageState == WebPageState.FINISHED_SUCCESSFUL
            PropertyChanges {
                target: _root
                color: "lightgreen"
            }
            PropertyChanges {
                target: _foundText
                text: totalResultsFound > 0
                      ? "Text entries found: " + String(totalResultsFound)
                      : "Couldn't find anything on web page"
                color: totalResultsFound > 0 ? "blue" : "red"
            }
        },
        State {
            when: _root.currentPageState == WebPageState.FINISHED_ERROR
            PropertyChanges {
                target: _root
                color: "red"
            }
            PropertyChanges {
                target: _foundText
                text: errorStringResolved
                color: "blue"
            }
        }
    ]
}
