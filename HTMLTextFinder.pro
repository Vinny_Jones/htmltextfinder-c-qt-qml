#
#   By Andrii Pyvovar
#

QT  += core gui qml

CONFIG += c++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = HTMLTextFinder
TEMPLATE = app

ICON = icon.icns

INCLUDEPATH += $$PWD/includes
INCLUDEPATH += $$PWD/includes/enums

SOURCES += sources/main.cpp \
    sources/networkConnection.cpp \
    sources/webPage.cpp \
    sources/mainLogic.cpp \
    sources/webPageModel.cpp

HEADERS += \
    includes/networkConnection.h \
    includes/webPage.h \
    includes/mainLogic.h \
    includes/qmlTypes.h \
    includes/webPageModel.h \
    includes/enums/webPageState.h \
    includes/enums/searchMode.h \
    includes/enums/currentState.h \
    includes/enums/domainmode.h

RESOURCES += \
    qml.qrc
