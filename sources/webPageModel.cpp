#include "webPageModel.h"

WebPageModel::WebPageModel(QObject *parent) : QAbstractListModel(parent)
{
    m_roles.reserve(WebPage::staticMetaObject.propertyCount());
    for (int i = 0; i < WebPage::staticMetaObject.propertyCount(); i++) {
        m_roles[i + WebPageModel::FirstPropertyRole] = WebPage::staticMetaObject.property(i).name();
    }
}

QVariant                    WebPageModel::data(const QModelIndex &modelIndex, int role) const
{
    if (!modelIndex.isValid()) {
        return QVariant();
    }

    int index = modelIndex.row();
    if (index < 0 || index >= m_data.size() || role < WebPageModel::FirstPropertyRole) {
        return QVariant();
    }
    return (WebPage::staticMetaObject.property(role - WebPageModel::FirstPropertyRole).read(m_data[index].get()));
}

QHash< int, QByteArray >    WebPageModel::roleNames() const {
    return m_roles;
}

int                         WebPageModel::rowCount(const QModelIndex &) const {
  return (m_data.size());   
}

void                        WebPageModel::clear() {
    beginResetModel();
    m_data.clear();
    endResetModel();
}

WebPage::Pointer &          WebPageModel::addElement(const QString & text, SearchMode::Type searchMode, bool caseInsensitive, const QString & url) {
    int index = m_data.size();
    beginInsertRows(QModelIndex(), index, index);
    m_data << std::make_shared<WebPage>(index, text, searchMode, caseInsensitive, url);
	QObject::connect(m_data.back().get(), SIGNAL(webPageDataChanged(int)), this, SLOT(dataElementChanged(int)));
    endInsertRows();
    return (m_data.last());
}

bool                        WebPageModel::contains(QString url) const {
    WebPage::trimUrlString(url);
    for (auto & it : m_data) {
        QString currentUrl = it->url();
        WebPage::trimUrlString(currentUrl);
        if (url == currentUrl)
            return (true);
    }
    return (false);
}

void                        WebPageModel::dataElementChanged(int index) {
    emit dataChanged(createIndex(index, 0), createIndex(index, 0));
}
