#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "qmlTypes.h"

int main(int argc, char *argv[])
{
	QGuiApplication app(argc, argv);
	QQmlApplicationEngine engine;

    /* Register types for qml */
    QmlTypes::registerTypes();

    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

	if (engine.rootObjects().isEmpty())
		return -1;
	return app.exec();
}
