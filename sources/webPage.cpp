#include "webPage.h"

WebPage::WebPage(int index, const QString & text, SearchMode::Type searchMode, bool caseInsensitive, const QString & url, QObject *parent)
    : QObject(parent),
    m_index(index),
	m_requestedText(text),
	m_searchMode(searchMode),
    m_caseInsensitive(caseInsensitive),
    m_url(url),
    m_resultsFound(0),
    m_state(WebPageState::Type::IDLE),
    m_connection(new NetworkConnection(url))    /* No need to delete it, since setAutoDelete will be set to true */
{
    m_connection->setAutoDelete(true);
    QObject::connect(m_connection, SIGNAL(finished(NetworkConnection::Result, QString)), this, SLOT(connectionFinished(NetworkConnection::Result, QString)));
    QObject::connect(m_connection, SIGNAL(started()), this, SLOT(setStarted()));
}

void    WebPage::trimUrlString(QString & url) {
    int index;

    clearUrlString(url);
    if ((index = url.indexOf("://")) >= 0)
        url.remove(0, index + 3);
    if (url.indexOf("www.") == 0)
        url.remove(0, 4);
    if ((index = url.indexOf('?')) >= 0)
        url.remove(index, url.size() - index);
    if ((index = url.indexOf('#')) >= 0)
        url.remove(index, url.size() - index);
}

void	WebPage::clearUrlString(QString & url) {
    url.remove(QChar('\"'));
    url.remove(QChar('\''));
    url.remove(QChar(' '));
}

QString	WebPage::getSiteDomain(const QString & url) {
    QString		result(url);
    if (result.indexOf("://") > 0)
        trimUrlString(result);
    int index = result.indexOf('/');
    if (index < 0)
        return (result);
    return (result.mid(0, result.indexOf('/')));
}

QString WebPage::url() const {
    return (m_url);
}

WebPageState::Type  WebPage::pageState() const {
    return (m_state);
}

int                 WebPage::resultsFound() const {
    return (m_resultsFound);
}

QString             WebPage::errorString() const {
    if (!m_errorString.isEmpty())
        return (m_errorString);
    return ("");
}

void    WebPage::setState(WebPageState::Type newValue) {
    if (m_state == newValue)
        return;
    m_state = newValue;
    if (isFinished())
        emit finished();
    emit webPageDataChanged(m_index);
}

bool    WebPage::isFinished() const {
    return(m_state == WebPageState::Type::FINISHED_SUCCESSFUL ||
           m_state == WebPageState::Type::FINISHED_ERROR);
}

void    WebPage::parseWebPage() {
    if (!m_connection.isNull()) {
        QThreadPool::globalInstance()->start(m_connection);
    }
}

void    WebPage::setPaused(bool paused) {
    if (m_connection == nullptr)
        return;
    m_connection->setPaused(paused);
}

void	WebPage::parseForText() {
    auto patternOptions = QRegularExpression::MultilineOption | (m_caseInsensitive
                                                                 ? QRegularExpression::CaseInsensitiveOption
                                                                 : QRegularExpression::NoPatternOption);

    QRegularExpression extract(resolveTargetTextPattern(), patternOptions);

    m_resultsFound = 0;
    QRegularExpressionMatchIterator it = extract.globalMatch(m_connectionReplyText);
	while (it.hasNext()) {
        m_resultsFound++;
        it.next();
	}

    if (m_resultsFound > 0)
        emit resultsFoundChanged();
}

QString WebPage::resolveTargetTextPattern() {
	QString		mainPattern("(?!([^<]+)?>)");
	if (m_searchMode == SearchMode::Type::WHOLE)
		return ("(" + m_requestedText + ")" + mainPattern);
	QStringList	splitted = m_requestedText.split(' ', QString::SplitBehavior::SkipEmptyParts, Qt::CaseInsensitive);
	QString	wordsForPattern;
	wordsForPattern.append('(');
	wordsForPattern.append(splitted.join('|'));
	wordsForPattern.append(')');

	return (wordsForPattern + mainPattern);
}

void    WebPage::parseForChildLinks() {
    QRegularExpression extractTag("<\\s*a([^>]+)>(.+?)<\\s*/\\s*a\\s*>",
                                    QRegularExpression::MultilineOption |
                                    QRegularExpression::CaseInsensitiveOption);

    QRegularExpression extractLinkFromTag("\\s*href\\s*=\\s*(\"([^\"]*\")|'[^']*'|([^'\">\\s]+))",
                                          QRegularExpression::CaseInsensitiveOption);

    QString url(m_url);
    QString protocol = m_url.left(m_url.indexOf("://"));
    trimUrlString(url);
    QString baseUrl = extractBaseUrl(extractLinkFromTag);       /* url of <base href=""> */
	QString rootDomain = getSiteDomain(url);
    QString currentUrl = (url.lastIndexOf('/') < 0) ? rootDomain : url.mid(0, url.lastIndexOf('/'));
    QRegularExpressionMatchIterator it = extractTag.globalMatch(m_connectionReplyText);

    while (it.hasNext()) {
        QRegularExpressionMatch matchLink = extractLinkFromTag.match(it.next().captured(1));
        if (matchLink.hasMatch()) {
            QString     strMatch = matchLink.captured(0).mid(6);
            QString     matchProtocol = (strMatch.indexOf("://") > 0 ? strMatch.left(strMatch.indexOf("://") + 3) : "");

			clearUrlString(matchProtocol);
            trimUrlString(strMatch);

            if (strMatch.isEmpty() || urlExceptionCases(strMatch)) {
                continue;
            }

            QString resolvedUrl;
            if (!matchProtocol.isEmpty()) {
                resolvedUrl = matchProtocol + strMatch;
            } else if (strMatch[0] == QChar('/')) {
				resolvedUrl = protocol + "://" + (strMatch.length() > 1 ? rootDomain + strMatch : rootDomain);
            } else {
                resolvedUrl = protocol + "://" + ((baseUrl.isEmpty() ? currentUrl : baseUrl) + '/' + strMatch);
            }
			if (resolvedUrl[resolvedUrl.size() - 1] == '/')
				resolvedUrl.chop(1);
            emit childFound(std::move(resolvedUrl));
        }
    }
}

/* Handles <base href="..."> */
QString WebPage::extractBaseUrl(QRegularExpression & extractLinkFromTag) {
    QRegularExpression extractBase("<\\s*base([^>]+)>",
                                   QRegularExpression::MultilineOption |
                                   QRegularExpression::CaseInsensitiveOption);
    QString baseHref;

    QRegularExpressionMatch matchBaseTag = extractBase.match(m_connectionReplyText);
    if (matchBaseTag.hasMatch()) {
        QRegularExpressionMatch matchBase = extractLinkFromTag.match(matchBaseTag.captured(0));
        if (matchBase.hasMatch()) {
            baseHref = matchBase.captured(0).mid(6);
            trimUrlString(baseHref);
        }
    }
    return (baseHref);
}

void     WebPage::setStarted() {
    setState(WebPageState::Type::STARTED);
}

void    WebPage::connectionFinished(NetworkConnection::Result connectionResult, QString data) {
    m_connection = Q_NULLPTR;
    m_connectionReplyText = std::move(data);

    if (connectionResult == NetworkConnection::Result::ERROR || connectionResult == NetworkConnection::Result::TIMEOUT) {
        m_errorString = m_connectionReplyText;
        setState(WebPageState::Type::FINISHED_ERROR);
    } else {
        parseForText();
        parseForChildLinks();
        setState(WebPageState::Type::FINISHED_SUCCESSFUL);
    }
}

bool    WebPage::urlExceptionCases(QString & url) {
    return (
                url.contains(':')
                || url.contains("//")
                );
}
