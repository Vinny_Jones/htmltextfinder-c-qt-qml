#include "networkConnection.h"

NetworkConnection::NetworkConnection(const QUrl & url, QObject *parent) : QObject(parent), m_url(url), m_paused(false)
{}

void    NetworkConnection::run() {
    emit started();
    m_mutex.lock();

    QTimer timer;
    timer.setInterval(NetworkConnection::connectionTimeoutMS);
    timer.setSingleShot(true);

    QNetworkAccessManager   manager;
    QNetworkReply   *reply = manager.get(QNetworkRequest(m_url));
    QEventLoop  localLoop;
    QObject::connect(&manager, SIGNAL(finished(QNetworkReply*)), &localLoop, SLOT(quit()));
    QObject::connect(&timer, SIGNAL(timeout()), &localLoop, SLOT(quit()));

    timer.start();
    localLoop.exec();

    QString replyString;
    Result  result;

    if (reply->error() != QNetworkReply::NoError) {
        replyString = reply->errorString();
        result = Result::ERROR;
    } else if (!reply->isFinished()) {
        replyString = "Connection timed out";
        result = Result::TIMEOUT;
    } else {
        replyString = reply->readAll();
        result = Result::SUCCESS;
    }
    delete reply;
    emit finished(result, replyString);
    m_mutex.unlock();
}

void    NetworkConnection::setPaused(bool paused) {
    if (m_paused == paused)
        return;
    m_paused = paused;
    if (m_paused)
        m_mutex.lock();
    else
        m_mutex.unlock();
}
