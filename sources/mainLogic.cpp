#include "mainLogic.h"

MainLogic::MainLogic(QObject *parent) : QObject(parent),
    m_currentState(CurrentState::Type::IDLE),
    m_totalPages(0),
    m_finishedPages(0),
    m_finishedPagesWithResults(0),
    m_dataModel(std::make_unique<WebPageModel>())
{}

MainLogic::~MainLogic() {
    QThreadPool::globalInstance()->clear();
    QThreadPool::globalInstance()->waitForDone();
}

void                    MainLogic::registerQmlType(const char * uri, int major, int minor) {
    qmlRegisterSingletonType<MainLogic>(uri, major, minor, "MainLogic",
                                        [](QQmlEngine *engine, QJSEngine *scriptEngine) -> QObject *
    {
        Q_UNUSED(engine)
        Q_UNUSED(scriptEngine)

        MainLogic *example = new MainLogic();
        return example;
    });
}

void                    MainLogic::openLinkInBrowser(const QString & url) const {
    QDesktopServices::openUrl(QUrl(url));
}

void                    MainLogic::startSearch(const QString & text, const QString & initialUrl) {
    emit totalPagesChanged((m_totalPages = 0));
    emit finishedPagesChanged((m_finishedPages = 0));
    emit finishedPagesWithResultsChanged((m_finishedPagesWithResults = 0));
    toStartState("Started");
	m_targetText = text;
	m_siteDomain = WebPage::getSiteDomain(initialUrl);
	proceedNewChild(initialUrl);
    setCurrentState(CurrentState::Type::RUNNING);
}

void                    MainLogic::toStartState(QString message, bool clearModel) {
    for (auto & item : m_activePages)
        item->setPaused(false);                     /* run paused threads */
    m_activePages.clear();                          /* clear all parse pending items */
    QThreadPool::globalInstance()->clear();         /* clear all thread pool pending items ... */
    QThreadPool::globalInstance()->waitForDone();   /* ... and wait for already started to be finished */

    if (clearModel)
        m_dataModel->clear();
    setCurrentState(CurrentState::Type::IDLE);
    emit statusBarMessageChanged(message);
}

int                     MainLogic::maxThreadCount() const {
    return (QThreadPool::globalInstance()->maxThreadCount());
}

void                    MainLogic::setMaxThreadCount(int value) {
	QThreadPool	*pool = QThreadPool::globalInstance();

	if (pool->maxThreadCount() == value)
		return;
	pool->setMaxThreadCount(value);
    emit maxThreadCountChanged(value);
}

QAbstractListModel *    MainLogic::webPageModel(void) {
    return (m_dataModel.get());
}

CurrentState::Type      MainLogic::currentState() const {
    return (m_currentState);
}

void                    MainLogic::setCurrentState(CurrentState::Type value) {
    if (m_currentState == value)
        return;
    CurrentState::Type  previous = m_currentState;

    m_currentState = value;
    if (m_currentState == CurrentState::Type::RUNNING) {
        emit statusBarMessageChanged("Running");
        proceedBFSSearch();
    }

    /* Pause will work for pending threads, others will be finished normally */
    if (m_currentState == CurrentState::Type::PAUSE) {
        for (auto & item : m_activePages) {
            if (item->pageState() == WebPageState::Type::IDLE)
                item->setPaused(true);
        }
        emit statusBarMessageChanged("Paused");
    }
    if (previous == CurrentState::Type::PAUSE) {
        for (auto & item : m_activePages)
            item->setPaused(false);
        proceedBFSSearch();
    }
    emit currentStateChanged();
}

/* BFS: If all pages of current level are finished, start working on their children. */
void                    MainLogic::proceedBFSSearch() {
    while (!m_activePages.empty() && m_activePages.first()->isFinished()) {
        m_activePages.removeFirst();
    }
    if (m_activePages.empty()) {
        toStartState("Finished", false);
        return;
    }
    /* If any active items left - do nothing, otherwise start processing new level  */
    auto    it = m_activePages.begin();
    if ((*it)->pageState() != WebPageState::Type::IDLE)
        return;
    while (it != m_activePages.end()) {
		(*it++)->parseWebPage();
    }
}

void                    MainLogic::proceedNewChild(QString url) {
    if (m_totalPages >= m_maxPagesAmount)
        return;
	if (m_siteDomain != WebPage::getSiteDomain(url) && m_domainMode == DomainMode::Type::INTERNAL)
		return;
    if (m_dataModel->contains(url))
        return ;
	m_activePages << addElementToModel(url);
    m_totalPages++;
    emit totalPagesChanged(m_totalPages);
}

WebPage::Pointer        MainLogic::addElementToModel(const QString & url) {
     WebPage::Pointer createdWebPage = m_dataModel->addElement(m_targetText, m_searchMode, m_caseInsensitive, url);
     WebPage * pagePtr = createdWebPage.get();

     QObject::connect(pagePtr, &WebPage::finished, this, [this]() {
         if (m_finishedPages < m_totalPages)
            emit finishedPagesChanged(++m_finishedPages);
         proceedBFSSearch();
     });
     QObject::connect(pagePtr, &WebPage::childFound, this, &MainLogic::proceedNewChild);
     QObject::connect(pagePtr, &WebPage::resultsFoundChanged, this, [this]() {
         if (m_finishedPagesWithResults < m_totalPages)
            emit finishedPagesWithResultsChanged(++m_finishedPagesWithResults);
     });
     return (createdWebPage);
}
